package com.example.elenaskorodilo.autotesting2ordr;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.openqa.selenium.remote.DesiredCapabilities;

import java.net.URL;

import io.appium.java_client.AppiumDriver;
import io.appium.java_client.remote.MobileCapabilityType;
import io.appium.java_client.service.local.AppiumDriverLocalService;
import io.appium.java_client.service.local.AppiumServiceBuilder;
import io.appium.java_client.service.local.flags.GeneralServerFlag;

/**
 * Created by elenaskorodilo on 26.10.17.
 */

public class OrderTest {

    AppiumDriver driver;
    private AppiumDriverLocalService service;
    private  String serverAddress = "0.0.0.0";

    @Before
    public  void setUp() throws Exception {

        DesiredCapabilities serverCapabilities = new DesiredCapabilities();
        AppiumServiceBuilder builder = new AppiumServiceBuilder().withCapabilities(serverCapabilities);
        builder.usingAnyFreePort();
        builder.withIPAddress(serverAddress);
        builder.withArgument(GeneralServerFlag.SESSION_OVERRIDE);
        builder.withArgument(GeneralServerFlag.LOG_LEVEL, "warn");

        service = builder.build();
        service.start();

        DesiredCapabilities desiredCapabilities = new DesiredCapabilities();
        desiredCapabilities.setCapability(MobileCapabilityType.PLATFORM_NAME, "Android");
        desiredCapabilities.setCapability(MobileCapabilityType.DEVICE_NAME, "Nexus");
        desiredCapabilities.setCapability(MobileCapabilityType.PLATFORM_VERSION, "7.1.2");
        desiredCapabilities.setCapability("appPackage", "com.vitart.audiotour");
        desiredCapabilities.setCapability("appActivity", ".activities.SplashActivity");
        desiredCapabilities.setCapability("app", "http://10.0.1.20:8080/job/Uniguide/job/Uniguide_Android/lastSuccessfulBuild/artifact/app/build/outputs/apk/app-debug.apk");
//        desiredCapabilities.setCapability("app", "/Users/elenaskorodilo/Documents/uniguide.apk");

        URL url = new URL("http://127.0.0.1:4723/wd/hub");
        driver = new AppiumDriver(builder, desiredCapabilities);
    }

    @After
    public void tearDown() throws Exception {
        driver.quit();
        service.stop();
    }

    @Test
    public void ClickCell() throws InterruptedException {

    }

}
